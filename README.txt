The console UI should be userfriendly and self-explanatory. Read here if need help.

---> INPUT PHASE <---

- To set the format the program will requires:
-- Vouchers digits number
-- Vouchers encoding type format

- How the voucher code type must be generated

- How many vouchers must be generated

---> OUTPUT <---

- each row is a different voucher
- on last row you see how many vouchers generated / on how many requested
- signature