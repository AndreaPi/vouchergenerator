import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;

/**
 * VouchersManager Class provide all functions to create and generate vouchers
 */
public class VouchersManager
{

	/** Max attempt to try to generate an univocal value */
	private static final long MAX_ATTEMPT = 100000;

	/** The Constant voucher default format is used to fill voucher code auto-generated. */
	private String voucherDefaultFormat = null;

	/** The generator types is used in input phase to show the options. */
	private CustomClassType[] generatorTypes;

	/** The encoding types is used in input phase to show the options. */
	private CustomClassType[] encodingTypes;

	/** The vouchers set is needed to check if vouchers are unique. */
	private HashSet<String> vouchers;

	/** The digits number represent the choosen format of vouchers. */
	private int digitsNumber;

	/** The digits number represent the range of digits used by encoding type. */
	private int digitsRange;

	/** The generator choosen by the user. */
	private Object generatorType;

	/** The encoding choosen by the user. */
	private Object encodingType;

	/** The getNextValue method of the generator. */
	private Method getNextVoucherValue;

	/** The convert method of the encoding. */
	private Method convert;



	/**
	 * Instantiates a new vouchers manager. Creates lists of all econding and generator types.
	 */
	public VouchersManager()
	{
		try
		{
			Class<?>[] encodingClasses = getClasses("encodings");
			this.encodingTypes = new CustomClassType[encodingClasses.length];
			this.encodingTypes = createTypeArray(encodingClasses);

			Class<?>[] generatorClasses = getClasses("generators");
			this.generatorTypes = new CustomClassType[generatorClasses.length];
			this.generatorTypes = createTypeArray(generatorClasses);
		} catch (Exception e)
		{
			System.out.print(e);
		}
	}



	/**
	 * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
	 * 
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private static Class<?>[] getClasses(String packageName)
			throws ClassNotFoundException, IOException
	{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements())
		{
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		for (File directory : dirs)
		{
			classes.addAll(findClasses(directory, packageName));
		}
		return classes.toArray(new Class[classes.size()]);
	}



	/**
	 * Recursive method used to find all classes in a given directory and subdirs.
	 * 
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException
	{
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists())
		{
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files)
		{
			if (file.isDirectory())
			{
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class"))
			{
				classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}



	/**
	 * Generates the CustomClassType array from Class<?> array.
	 * 
	 * @param CustomClassType
	 *            array
	 * 
	 * @return the Class<?> array
	 */
	private CustomClassType[] createTypeArray(Class<?>[] customClassArray)
	{
		CustomClassType[] customClassType = new CustomClassType[customClassArray.length];
		Class<?> typeGenerator;
		Method getName;
		Method getLabel;
		Object stringName = null;
		Object charLabel = null;
		for (int i = 0; i < customClassArray.length; i++)
		{
			try
			{
				typeGenerator = Class.forName(customClassArray[i].getName());
				getName = typeGenerator.getMethod("getName");
				getLabel = typeGenerator.getMethod("getLabel");
				stringName = getName.invoke(null);
				charLabel = getLabel.invoke(null);
			} catch (Exception e)
			{
				System.out.print(e);
			}

			CustomClassType voucherType = new CustomClassType((char) charLabel, (String) stringName, customClassArray[i]);
			customClassType[i] = voucherType;
		}
		return customClassType;
	}



	/**
	 * Initialize the generator of the type of the parameter.
	 * 
	 * @param encodingType
	 *            the type of coding
	 * @param numericGeneratorType
	 *            the type of generator
	 * @param digitsNumber
	 *            the number of digits
	 */
	public void initializeGenerator(CustomClassType encodingType, CustomClassType numericGeneratorType, int digitsNumber)
	{
		this.vouchers = new HashSet<String>();
		this.digitsNumber = digitsNumber;
		try
		{
			Class<?> customClassType = encodingType.getClassType();
			this.encodingType = customClassType.getConstructor().newInstance();
			this.convert = customClassType.getDeclaredMethod("convert", long.class);
			Method getDigitsRange = customClassType.getDeclaredMethod("getDigitsRange");
			this.digitsRange = (int) getDigitsRange.invoke(this.encodingType);

			this.voucherDefaultFormat = new String(new char[getMaxDigits()]).replace('\0', '0');

			customClassType = numericGeneratorType.getClassType();
			this.generatorType = customClassType.getConstructor(long.class).newInstance(getMaxValue());
			this.getNextVoucherValue = customClassType.getDeclaredMethod("getNextVoucherValue");

		} catch (Exception e)
		{
			System.out.print(e);
		}

	}



	/**
	 * Composes more vouchers to bypass the max size.
	 * 
	 * @return voucher or null if failed
	 */
	public String getNextVoucher()
	{
		String fullVoucherCode = "";
		while (fullVoucherCode.length() < this.digitsNumber)
		{
			String subVoucherCode = generateSubVoucherCode();
			if (subVoucherCode == null)
			{
				return null;
			}
			else
			{
				fullVoucherCode = fullVoucherCode.concat(subVoucherCode);
			}
		}
		String end = fullVoucherCode.substring(fullVoucherCode.length() - this.digitsNumber);
		return end;
	}



	/**
	 * Generate voucher from a number that must be converted to alphanumeric code and filled with zeros. Vouchers are
	 * unique and have a max size.
	 * 
	 * @return voucher or null if failed
	 */
	private String generateSubVoucherCode()
	{
		try
		{
			for (int i = 0; i < MAX_ATTEMPT; i++)
			{
				long numberGenerated = (long) this.getNextVoucherValue.invoke(this.generatorType);
				String rawVoucherCode = (String) this.convert.invoke(this.encodingType, numberGenerated);
				String formattedVoucherCode = (this.voucherDefaultFormat + rawVoucherCode).substring(rawVoucherCode.length());
				if (!this.vouchers.contains(formattedVoucherCode))
				{
					this.vouchers.add(formattedVoucherCode);
					return formattedVoucherCode;
				}
			}
			return null;
		} catch (Exception e)
		{
			System.out.print(e);
			return null;
		}
	}



	/**
	 * Calculates and the maximum possible number of digits for vouchers to avoid overflow, compare with digits value on
	 * user input and return the lower.
	 * 
	 * @param encodingType
	 * 
	 * @return the max digits value
	 */
	public int getMaxDigits()
	{
		int n = 0;
		while (Long.MAX_VALUE != (long) (Math.pow(this.digitsRange, n) * 2))
		{
			n++;
		}
		n--;
		if (n < this.digitsNumber)
		{
			return n;
		}
		else
		{
			return this.digitsNumber;
		}
	}



	/**
	 * Calculates and gets the maximum possible value of vouchers.
	 * 
	 * @return the max value
	 */
	public long getMaxValue()
	{
		return (long) Math.pow(this.digitsRange, this.voucherDefaultFormat.length());
	}



	/**
	 * Gets the list of voucher types.
	 * 
	 * @return the voucher types
	 */
	public CustomClassType[] getGeneratorTypes()
	{
		return this.generatorTypes;
	}



	/**
	 * Gets the list of voucher types.
	 * 
	 * @return the voucher types
	 */
	public CustomClassType[] getEncodingTypes()
	{
		return this.encodingTypes;
	}

}
