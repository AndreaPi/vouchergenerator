/**
 * The Class VoucherType define a type of voucher.
 */
public class CustomClassType
{
	
	/** The name. */
	private String name;
	
	/** The label used for selection. */
	private char label;
	
	/** The class type (generator used). */
	private Class<?> classType;



	/**
	 * The builder.
	 *
	 * @param label
	 * @param description
	 * @param generator type
	 */
	public CustomClassType(char label, String description, Class<?> classType)
	{
		this.label = label;
		this.name = description;
		this.classType = classType;
	}



	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public char getLabel()
	{
		return this.label;
	}



	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}



	/**
	 * Gets the class type.
	 *
	 * @return the class type
	 */
	public Class<?> getClassType()
	{
		return this.classType;
	}

}
