package interfaces;
/**
 * The Interface NumberGenerator describes the generic generator.
 */
public interface GeneratorType
{

	/** The name of the generator displayed in input phase. */
	static final String NAME = null;

	/** The label of the generator used for the selection. */
	static final char LABEL = 0;



	/**
	 * Gets the name. Available only on java 8, previous versions doesn't support static methods in interface.
	 * 
	 * @return the name
	 */
	// public static String getName();

	/**
	 * Gets the label. Available only on java 8, previous versions doesn't support static methods in interface.
	 * 
	 * @return the name
	 */
	// public static char getLabel();

	/**
	 * Gets the next value.
	 * 
	 * @return the next value
	 */
	public long getNextVoucherValue();
}
