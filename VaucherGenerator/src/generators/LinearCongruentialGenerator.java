package generators;

import interfaces.GeneratorType;

/**
 * The Class LinearCongruentialGenerator generates pseudo-random numbers using the formula x = (a * x + c) % m, where, x
 * is the seed, a the multiplier, c the increment and m the modulus.
 */
public class LinearCongruentialGenerator implements GeneratorType
{

	/** @see GeneratorType#NAME */
	private static final String NAME = "random congruential";

	/** @see GeneratorType#LABEL */
	private static final char LABEL = 'r';

	/** Default values for x, a, c, m if not defined in the builder */
	private static final long X = 27;
	private static final long A = 1220703125;
	private static final long C = 0;
	private static final long M = 214748648;

	/** @see GeneratorType#x */
	private long x;

	/** values of a, c, m parameters, used to generate x */
	private long a, c, m;



	/**
	 * Instantiates a new random generator.
	 * 
	 * @param x
	 * @param a
	 * @param c
	 * @param m
	 */
	public LinearCongruentialGenerator(long x, long a, long c, long m)
	{
		this.x = x;
		this.a = a;
		this.c = c;
		this.m = m;
	}



	/**
	 * Instantiates a new random generator.
	 */
	public LinearCongruentialGenerator(long max)
	{
		this(X, A, C, findLowestMayorThan(max));
	}



	/**
	 * Instantiates a new random generator.
	 */
	public LinearCongruentialGenerator()
	{
		this(X, A, C, M);
	}



	/**
	 * Finds the lowest possible 2^n value, higher than input.
	 * 
	 * @param value
	 * 
	 * @return result
	 */
	private static long findLowestMayorThan(long value)
	{
		long targetModulus = 2;
		while (value > targetModulus)
		{
			targetModulus *= 2;
		}
		return targetModulus;
	}



	/**
	 * @see GeneratorType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see GeneratorType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see GeneratorType#getNextVoucherValue()
	 */
	@Override
	public long getNextVoucherValue()
	{
		this.x = (this.a * this.x + this.c) % this.m;
		return this.x;
	}



	/**
	 * Gets the multiplier.
	 * 
	 * @return the multiplier
	 */
	public long getMultiplier()
	{
		return this.a;
	}



	/**
	 * Gets the increment.
	 * 
	 * @return the increment
	 */
	public long getIncrement()
	{
		return this.c;
	}



	/**
	 * Gets the modulus.
	 * 
	 * @return the modulus
	 */
	public long getModulus()
	{
		return this.m;
	}

}
