package generators;

import interfaces.GeneratorType;

/**
 * The Class KErlangGenerator generates a random number with k-erlang trend using exponential generator as base.
 */
public class KErlangGenerator implements GeneratorType
{

	/** @see GeneratorType#NAME */
	private static final String NAME = "random k-erlang";

	/** @see GeneratorType#LABEL */
	private static final char LABEL = 'k';

	/** The Constant DEFAULT_K used in case k is not declared. */
	private final static int DEFAULT_K = 3;

	/** The exponential generator used as base. */
	private ExponentialGenerator exponentialGenerator;

	/** The k is the number of exponential values that are used to create a k-erlang value. */
	private int k;

	/** The ts rapresents the average . */
	private double ts;



	/**
	 * Instantiates a new k-erlang generator.
	 * 
	 * @param exponentialGenerator
	 *            the exponential generator
	 * @param k
	 *            the number of exponential values
	 * @param ts
	 *            the average
	 */
	public KErlangGenerator(ExponentialGenerator exponentialGenerator, int k, double ts)
	{
		this.exponentialGenerator = exponentialGenerator;
		this.k = k;
		this.ts = ts;

	}



	/**
	 * Instantiates a new k-erlang generator.
	 * 
	 * @param max
	 *            the maximum value
	 */
	public KErlangGenerator(long max)
	{
		this(new ExponentialGenerator((double) max / DEFAULT_K), DEFAULT_K, (double) max / DEFAULT_K);
	}



	/**
	 * @see GeneratorType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see GeneratorType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see GeneratorType#getNextVoucherValue()
	 */
	@Override
	public long getNextVoucherValue()
	{
		return (long) getNextNumber();
	}



	/**
	 * Generate and gets the next number.
	 * 
	 * @return the next number
	 */
	public double getNextNumber()
	{
		double exponentialSum = 0;
		for (int i = 0; i < this.k; i++)
		{
			exponentialSum += this.exponentialGenerator.getNextNumber();
		}
		return exponentialSum;
	}



	/**
	 * Gets the exponential generator.
	 * 
	 * @return the exponential generator
	 */
	public ExponentialGenerator getExponentialGenerator()
	{

		return this.exponentialGenerator;
	}



	/**
	 * Gets the average.
	 * 
	 * @return the average
	 */
	public double getTs()
	{
		return this.ts;
	}



	/**
	 * Gets the k.
	 * 
	 * @return the k
	 */
	public double getK()
	{
		return this.k;
	}

}
