package generators;

import interfaces.GeneratorType;

/**
 * The Class ExponentialGenerator generates a random number with exponential trend using uniform generator(0,1) as base.
 */
public class ExponentialGenerator implements GeneratorType
{

	/** @see GeneratorType#NAME */
	private static final String NAME = "random exponential";

	/** @see GeneratorType#LABEL */
	private static final char LABEL = 'e';

	/** The uniform generator used. */
	private UniformGenerator uniformGenerator;

	/** The lambda. */
	private double lambda;



	/**
	 * Instantiates a new exponential generator.
	 * 
	 * @param uniformGenerator
	 *            the uniform generator used
	 * @param lambda
	 */
	public ExponentialGenerator(UniformGenerator uniformGenerator, double lambda)
	{
		this.lambda = lambda;
		this.uniformGenerator = uniformGenerator;

	}



	/**
	 * Instantiates a new exponential generator.
	 * 
	 * @param lambda
	 */
	public ExponentialGenerator(double lambda)
	{
		this(new UniformGenerator(), lambda);
	}



	/**
	 * Instantiates a new exponential generator.
	 * 
	 * @param max
	 *            value = lambda
	 */
	public ExponentialGenerator(long max)
	{
		this(new UniformGenerator(), (double) max);
	}



	/**
	 * @see GeneratorType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see GeneratorType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see GeneratorType#getNextVoucherValue()
	 */
	@Override
	public long getNextVoucherValue()
	{
		return (long) getNextNumber();
	}



	/**
	 * Generates and gets the next number.
	 * 
	 * @return the next number
	 */
	public double getNextNumber()
	{
		return -1.0 * this.getLambda() * Math.log(this.uniformGenerator.getNextNumber());
	}



	/**
	 * Gets the lambda.
	 * 
	 * @return the lambda
	 */
	public double getLambda()
	{
		return this.lambda;
	}



	/**
	 * Gets the uniform generator.
	 * 
	 * @return the uniform generator
	 */
	public UniformGenerator getUniformGenerator()
	{
		return this.uniformGenerator;
	}

}
