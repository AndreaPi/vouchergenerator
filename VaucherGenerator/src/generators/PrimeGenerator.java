package generators;
import interfaces.GeneratorType;

import java.util.ArrayList;

/**
 * The Class PrimeGenerator generates prime numbers.
 */
public class PrimeGenerator implements GeneratorType
{

	/** @see GeneratorType#NAME */
	private static final String NAME = "prime";

	/** @see GeneratorType#LABEL */
	private static final char LABEL = 'p';

	/** The prime numbers set. */
	private ArrayList<Long> primeNumbers;

	/** The x value. */
	private long x = 1;



	/**
	 * Instantiates a new prime generator.
	 * 
	 * @param max
	 *            the max value
	 */
	public PrimeGenerator(long max)
	{
		this.primeNumbers = new ArrayList<Long>();
	}



	/**
	 * @see GeneratorType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see GeneratorType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see GeneratorType#getNextVoucherValue()
	 */
	@Override
	public long getNextVoucherValue()
	{
		while (true)
		{
			this.x++;
			if (isPrimeNumber(this.x))
			{
				primeNumbers.add(this.x);
				return this.x;
			}
		}
	}



	private boolean isPrimeNumber(long number)
	{
		for (int i = 0; i < this.primeNumbers.size(); i++)
		{
			if (number % this.primeNumbers.get(i) == 0)
			{
				return false;
			}
		}
		return true;
	}

}
