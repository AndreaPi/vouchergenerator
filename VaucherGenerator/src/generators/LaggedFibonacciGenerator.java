package generators;
import interfaces.GeneratorType;

import java.util.ArrayList;


/**
 * The Class LaggedFibonacciGenerator generates random values based on Fibonacci succession: x = (xj+xk) mod (m), where 0 < j < k.
 */
public class LaggedFibonacciGenerator implements GeneratorType
{

	/** @see GeneratorType#NAME */
	private static final String NAME = "random fibonacci";

	/** @see GeneratorType#LABEL */
	private static final char LABEL = 'f';
	
	/** The Constants X1 and X2 are the 1st and 2nd elements of the succession. */
	private static final long X1 = 1, X2 = 2;
	
	/** The uniform generator used to random choose j and k indexes. */
	private UniformGenerator uniformGenerator;
	
	/** The Fibonacci succession. */
	private ArrayList<Long> fibonacciSuccession;

	/** The modulus. */
	private long m;



	/**
	 * Instantiates a new lagged Fibonacci generator.
	 *
	 * @param uniformGenerator the uniform generator
	 * @param x1 the 1st element
	 * @param x2 the 2n element
	 * @param m the modulus
	 */
	public LaggedFibonacciGenerator(UniformGenerator uniformGenerator, long x1, long x2, long m)
	{
		this.uniformGenerator = uniformGenerator;
		this.fibonacciSuccession = new ArrayList<Long>();
		this.fibonacciSuccession.add(x1);
		this.fibonacciSuccession.add(x2);
		this.m = m;
	}



	/**
	 * Instantiates a new lagged Fibonacci generator.
	 *
	 * @param max the maximum value
	 */
	public LaggedFibonacciGenerator(long max)
	{
		this(new UniformGenerator(), X1, X2, findLowestMayorThan(max));
	}



	/**
	 * Finds the lowest possible 2^n value, higher than input.
	 * 
	 * @param value
	 * 
	 * @return result
	 */
	private static long findLowestMayorThan(long value)
	{
		long targetModulus = 2;
		while (value > targetModulus)
		{
			targetModulus *= 2;
		}
		return targetModulus;
	}



	/**
	 * @see GeneratorType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see GeneratorType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see GeneratorType#getNextVoucherValue()
	 */
	@Override
	public long getNextVoucherValue()
	{
		int k = (int) (this.uniformGenerator.getNextNumber() * (this.fibonacciSuccession.size() - 1) + 1);
		int j = (int) (this.uniformGenerator.getNextNumber() * this.uniformGenerator.getNextNumber() * k);
		long x = (this.fibonacciSuccession.get(j) + this.fibonacciSuccession.get(k)) % this.m;
		this.fibonacciSuccession.add(x);
		return x;
	}

}
