package generators;

import interfaces.GeneratorType;

/**
 * The Class SequentialGenerator generates sequential numbers.
 */
public class SequentialGenerator implements GeneratorType
{

	/** @see GeneratorType#NAME */
	private static final String NAME = "sequential";

	/** @see GeneratorType#LABEL */
	private static final char LABEL = 's';

	/** The x value. */
	private long x = 0;



	/**
	 * Instantiates a new sequential generator.
	 * 
	 * @param max
	 *            the max value
	 */
	public SequentialGenerator(long max)
	{
	}



	/**
	 * @see GeneratorType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see GeneratorType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see GeneratorType#getNextVoucherValue()
	 */
	@Override
	public long getNextVoucherValue()
	{
		this.x++;
		return this.x;
	}

}
