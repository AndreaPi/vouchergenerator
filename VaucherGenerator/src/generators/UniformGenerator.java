package generators;

import interfaces.GeneratorType;

/**
 * The Class UniformGenerator generates a random number between a minimum and a maximum value based on a linear
 * congruential generator.
 */
public class UniformGenerator implements GeneratorType
{

	/** @see GeneratorType#NAME */
	private static final String NAME = "random uniform";

	/** @see GeneratorType#LABEL */
	private static final char LABEL = 'u';

	/** The default minimum and maximum values if not passed. */
	private static final double DEFAULT_MIN = 0, DEFAULT_MAX = 1;

	/** The Linear congruential generator used. */
	private LinearCongruentialGenerator LinearCongruentialGenerator;

	/** The minimum and maximum values. */
	private double min, max;



	/**
	 * Instantiates a new uniform generator.
	 * 
	 * @param randomGenerator
	 *            the random generator
	 * @param min
	 *            the minimum value
	 * @param max
	 *            the maximum value
	 */
	public UniformGenerator(LinearCongruentialGenerator randomGenerator, double min, double max)
	{
		this.min = min;
		this.max = max;
		this.LinearCongruentialGenerator = randomGenerator;
	}



	/**
	 * Instantiates a new uniform generator.
	 * 
	 * @param min
	 *            the minimum value
	 * @param max
	 *            the maximum value
	 *//*
	public UniformGenerator(double min, double max)
	{
		this(new LinearCongruentialGenerator(), min, max);
	}

*/

	/**
	 * Instantiates a new uniform generator.
	 * 
	 * @param max
	 *            the maximum value
	 */
	public UniformGenerator(long max)
	{
		this(new LinearCongruentialGenerator(), DEFAULT_MIN, (double) max);
	}



	/**
	 * Instantiates a new uniform generator.
	 */
	public UniformGenerator()
	{
		this(new LinearCongruentialGenerator(), DEFAULT_MIN, DEFAULT_MAX);
	}



	/**
	 * @see GeneratorType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see GeneratorType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see GeneratorType#getNextVoucherValue()
	 */
	@Override
	public long getNextVoucherValue()
	{
		return (long) getNextNumber();
	}



	/**
	 * Generates and gets the next number.
	 * 
	 * @return the next number
	 */
	public double getNextNumber()
	{
		return this.min + (this.max - this.min) * ((double) this.LinearCongruentialGenerator.getNextVoucherValue() / ((double) this.LinearCongruentialGenerator.getModulus()));
	}



	/**
	 * Gets the minimum value.
	 * 
	 * @return the minimum value
	 */
	public double getMin()
	{
		return this.min;
	}



	/**
	 * Gets the maximum value.
	 * 
	 * @return the maximum value
	 */
	public double getMax()
	{
		return this.max;
	}



	/**
	 * Gets the linear congruential generator.
	 * 
	 * @return the linear congruential generator
	 */
	public LinearCongruentialGenerator getLinearCongruentialGenerator()
	{
		return this.LinearCongruentialGenerator;
	}
}
