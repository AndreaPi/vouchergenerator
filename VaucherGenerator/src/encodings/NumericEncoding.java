package encodings;

import interfaces.EncodingType;

/**
 * The Class NumericEncoding convert and long input into the corresponding String(0-1).
 */

public class NumericEncoding implements EncodingType
{

	/** @see EncodingType#NAME */
	private static final String NAME = "numeric";

	/** @see EncodingType#LABEL */
	private static final char LABEL = 'n';

	/** @see EncodingType#DIGITS_RANGE */
	static final char DIGITS_RANGE = 10;



	/**
	 * Instantiates a new numeric encoding.
	 */
	public NumericEncoding()
	{
	}



	/**
	 * @see EncodingType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see EncodingType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see EncodingType#getDigitsRange()
	 */
	public static int getDigitsRange()
	{
		return DIGITS_RANGE;
	}



	/**
	 * @see EncodingType#convert(long number)
	 */
	@Override
	public String convert(long number)
	{
		return Long.toString(number);
	}
}