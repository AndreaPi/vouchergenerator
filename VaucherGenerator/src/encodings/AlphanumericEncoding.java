package encodings;

import interfaces.EncodingType;

/**
 * The Class AlphanumericEncoding convert and long input into the corresponding String( 0-1 + A-Z ).
 */
public class AlphanumericEncoding implements EncodingType
{

	/** @see EncodingType#NAME */
	private static final String NAME = "alpha-numeric";

	/** @see EncodingType#LABEL */
	private static final char LABEL = 'a';

	/** @see EncodingType#DIGITS_RANGE */
	static final char DIGITS_RANGE = 36;



	/**
	 * Instantiates a new alpha-numeric encoding.
	 */
	public AlphanumericEncoding()
	{
	}



	/**
	 * @see EncodingType#getName()
	 */
	public static String getName()
	{
		return NAME;
	}



	/**
	 * @see EncodingType#getLabel()
	 */
	public static char getLabel()
	{
		return LABEL;
	}



	/**
	 * @see EncodingType#getDigitsRange()
	 */
	public static int getDigitsRange()
	{
		return DIGITS_RANGE;
	}



	/**
	 * @see EncodingType#convert(long number)
	 */
	@Override
	public String convert(long number)
	{
		char letter;
		long quot = number / DIGITS_RANGE;
		long rem = number % DIGITS_RANGE;

		if (rem < 10)
		{
			letter = (char) ((int) '0' + rem);
		}
		else
		{
			letter = (char) ((int) 'A' + rem - 10);
		}

		if (quot == 0)
		{
			return "" + letter;
		}
		else
		{
			return this.convert(quot) + letter;
		}
	}
}
