/**
 * Author: Andrea Picciolo
 */

import java.util.Scanner;

/**
 * The Class Main.
 */
public class Main
{

	/** The error text message. */
	private static final String ERROR_TEXT = " not valid, try again\n\n";

	/** The reader. */
	private static Scanner READER;

	/** The vouchers manager. */
	private static VouchersManager VOUCHERS_MANAGER;



	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		new Main();
	}



	/**
	 * Main.
	 */
	public Main()
	{ 	
		// READ PHASE
		System.out.print("--> START <--\n\n");
		// Instantiates the manager
		VOUCHERS_MANAGER = new VouchersManager();
		// Reads number of digits
		int digitsNumber = (int) readNumber(Integer.MAX_VALUE, "digits");
		// Reads the encoding type
		CustomClassType[] encodingTypes = VOUCHERS_MANAGER.getEncodingTypes();
		CustomClassType encodingType = readType(encodingTypes, "encoding");
		// Read the generator type
		CustomClassType[] generatorTypes = VOUCHERS_MANAGER.getGeneratorTypes();
		CustomClassType generatorType = readType(generatorTypes, "voucher");
		// Initializes the manager
		VOUCHERS_MANAGER.initializeGenerator(encodingType, generatorType, digitsNumber);
		// Reads the quantity of vouchers
		long vouchersQuantity = readNumber(VOUCHERS_MANAGER.getMaxValue(), "vouchers quantity");
		// OUTPUT PHASE
		System.out.print("\n--> OUTPUT <--\n\n");
		// Generates the vouchers
		GenerateVouchers(vouchersQuantity);
		// END
		System.out.print("\n--> END <--\n\nThanks for using\nAndrea Picciolo");
	}



	/**
	 * Read a type from keyboard input.
	 * 
	 * @return the char
	 */
	private CustomClassType readType(CustomClassType[] type, String text)
	{
		while (true)
		{
			System.out.print("Choose the " + text + " type:\n");
			for (int i = 0; i < type.length; i++)
			{
				System.out.print(type[i].getLabel() + ": [" + type[i].getName() + " " + text + "]\n");
			}
			try
			{
				READER = new Scanner(System.in);
				String inputLine = READER.nextLine();
				for (int i = 0; i < type.length; i++)
				{
					if ((inputLine.charAt(0) == type[i].getLabel()) || (inputLine.charAt(0) == (char) (type[i].getLabel() - 32)))
					{
						return type[i];
					}
				}
			} catch (Exception e)
			{
			}
			System.out.print(beginningCapitalLetter(text) + ERROR_TEXT);
		}
	}



	/**
	 * Read a number from keyboard input.
	 * 
	 * @return the number
	 */
	private long readNumber(long max, String text)
	{
		while (true)
		{
			System.out.print("Insert " + text + " number (max " + max + "):\n");
			READER = new Scanner(System.in);
			try
			{
				long number = Long.parseLong(READER.nextLine());
				if ((number > max) || (number < 1))
				{
					System.out.print(beginningCapitalLetter(text) + ERROR_TEXT);
				}
				else
				{
					return number;
				}
			} catch (Exception e)
			{
				System.out.print(beginningCapitalLetter(text) + ERROR_TEXT);
			}
		}
	}



	/**
	 * Generate vouchers.
	 * 
	 * @param voucherType
	 * @param voucherQuantity
	 */
	private void GenerateVouchers(long voucherQuantity)
	{
		int numberVouchersGenerated = 0;
		String voucher;

		for (int i = 0; i < voucherQuantity; i++)
		{
			voucher = VOUCHERS_MANAGER.getNextVoucher();
			if (voucher != null)
			{
				numberVouchersGenerated++;
				System.out.print(voucher + "\n");
			}
		}
		System.out.print(numberVouchersGenerated + "/" + voucherQuantity + " vouchers generated\n");
	}



	/**
	 * Trasform the 1st letter into capital letter.
	 * 
	 * @param a
	 *            word
	 * @return the same word with 1st letter as capital letter
	 */
	private String beginningCapitalLetter(String word)
	{
		return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
	}
}
